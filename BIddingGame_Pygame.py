import pygame
import random

# Initialize Pygame
pygame.init()

# Set up the display (window)
screen_width, screen_height = 800, 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Card Game")

# Define colors
GREEN = (13, 168, 65)

# Card class
class Card:
    def __init__(self, back_image, front_image, pos):
        self.back_image_original = pygame.image.load(back_image)
        self.front_image_original = pygame.image.load(front_image)

        self.back_image = pygame.transform.scale(self.back_image_original, (screen_width // 6, screen_height // 3))
        self.front_image = pygame.transform.scale(self.front_image_original, (screen_width // 6, screen_height // 3))
        
        self.pos = pos
        self.face_up = False

    def draw(self):
        if self.face_up:
            screen.blit(self.front_image, self.pos)
        else:
            screen.blit(self.back_image, self.pos)

    def flip(self):
        self.face_up = not self.face_up

# Deck class
class Deck:
    def __init__(self, card_paths):
        self.i = -1
        self.cards = []
        self.flipped_card = None  # Track the currently flipped card
        self.list_flipped_cards = []

        self.user_cards = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'j': 10, 'q': 10, 'k': 10, 'a': 10}
        self.computer_cards = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'j': 10, 'q': 10, 'k': 10, 'a': 10}

        self.score_user = 0
        self.score_computer = 0
        self.banker_cards = []
        for path in card_paths:
            card = Card(path[0], path[1], (100, 200))  # Adjust position as needed
            self.cards.append(card)
            self.banker_cards.append(path[1].split('_')[1][5:])
        self.banker_cards_values = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'jack': 10, 'queen': 10, 'king': 10, 'ace': 10}

    def draw(self):
        for card in self.cards:
            card.draw()
        for card in self.list_flipped_cards:
            card.draw()

    def handle_click(self, pos):
        # Check if there's a flipped card and if it was clicked
        if self.flipped_card and self.flipped_card.pos[0] <= pos[0] <= self.flipped_card.pos[0] + self.flipped_card.front_image.get_width() and \
                self.flipped_card.pos[1] <= pos[1] <= self.flipped_card.pos[1] + self.flipped_card.front_image.get_height():
            self.i += 1
            while True:
                user_input = input("Pick a card to bid: ").lower()
                if user_input in self.user_cards:
                    print("Bidded with", user_input)
                    self.bid_win(user_input)
                    break
                else:
                    print("Invalid Card!!!, Your cards are", list(self.user_cards.keys()))
            
            # Move the flipped card to a new position
            self.flipped_card.pos = (self.flipped_card.pos[0] + 200 + self.i * 5, self.flipped_card.pos[1])  # Adjust the new position as needed
            self.list_flipped_cards.append(self.flipped_card)
            self.flipped_card = None  # Reset flipped_card to None
            
        else:
            for card in reversed(self.cards):
                if card.face_up:
                    continue  # Skip face-up cards
                if card.pos[0] <= pos[0] <= card.pos[0] + card.front_image.get_width() and \
                        card.pos[1] <= pos[1] <= card.pos[1] + card.front_image.get_height():
                    card.flip()
                    self.flipped_card = card  # Set flipped_card to the clicked card
                    break

    def bid_win(self, user_choice):
        computer_choice = random.choice(list(self.computer_cards.keys()))
        print('Computer bidded with: ', computer_choice)
        if self.user_cards[user_choice] > self.computer_cards[computer_choice]:
            self.score_user += self.banker_cards_values[self.banker_cards[-1]]
            self.banker_cards.pop(-1)
        elif self.user_cards[user_choice] < self.computer_cards[computer_choice]:
            self.score_computer += self.banker_cards_values[self.banker_cards[-1]]
            self.banker_cards.pop(-1)
        else:
            score = self.banker_cards_values[self.banker_cards[-1]]
            self.score_computer += score // 2
            self.score_user += score // 2
            self.banker_cards.pop(-1)
        print("Computer Score:", self.score_computer)
        print("User Score:", self.score_user)
        del self.user_cards[user_choice]
        del self.computer_cards[computer_choice]
        if self.i == 12:
            if self.score_computer > self.score_user:
                print("You lost!!!!")
            elif self.score_computer < self.score_user:
                print("You Won!!!!")
            else:
                print("It's a Tie!!!!")

# Create 
card_paths = [
    ("card_imgs/hiding_side.jpeg", "card_imgs/2_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/3_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/4_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/5_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/6_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/7_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/8_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/9_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/10_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/jack_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/queen_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/king_of_diamonds.png"),
    ("card_imgs/hiding_side.jpeg", "card_imgs/ace_of_diamonds.png"),
]
random.shuffle(card_paths)
deck = Deck(card_paths)  # Adjust number of cards as needed

# Main game loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # Left mouse button
                deck.handle_click(pygame.mouse.get_pos())

    screen.fill(GREEN)
    deck.draw()
    pygame.display.flip()

pygame.quit()
